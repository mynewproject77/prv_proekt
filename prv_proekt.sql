-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 23, 2018 at 02:51 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prv_proekt`
--

-- --------------------------------------------------------

--
-- Table structure for table `proekt`
--

CREATE TABLE `proekt` (
  `id` int(11) NOT NULL,
  `ime_kompanija` varchar(255) NOT NULL,
  `link_cover` varchar(255) NOT NULL,
  `naslov` varchar(255) NOT NULL,
  `podnaslov` varchar(255) NOT NULL,
  `nesto_za_vas` text NOT NULL,
  `telefon` int(11) NOT NULL,
  `lokacija` varchar(255) NOT NULL,
  `url1` text NOT NULL,
  `opis_url1` text NOT NULL,
  `url2` text NOT NULL,
  `opis_url2` text NOT NULL,
  `url3` text NOT NULL,
  `opis_url3` text NOT NULL,
  `nesto_poveke` text NOT NULL,
  `najdole` text NOT NULL,
  `linked` varchar(255) NOT NULL,
  `face` varchar(255) NOT NULL,
  `twiter` varchar(255) DEFAULT NULL,
  `google` varchar(255) DEFAULT NULL,
  `servisi_produkti` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proekt`
--

INSERT INTO `proekt` (`id`, `ime_kompanija`, `link_cover`, `naslov`, `podnaslov`, `nesto_za_vas`, `telefon`, `lokacija`, `url1`, `opis_url1`, `url2`, `opis_url2`, `url3`, `opis_url3`, `nesto_poveke`, `najdole`, `linked`, `face`, `twiter`, `google`, `servisi_produkti`) VALUES
(1, 'vero100', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'dfdsfsdfsdf', 'fsdfsdfsdfdsfsdfds', 'sdfdsfsdfqew fewf wefwe efwe efawefewf', 12345678, 'byl koco racin', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'fvfdvfafvstght argg gtgsgt', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'erg regergerga rgfrege', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'erfgaeg erg rgar rg erg e', 'r grtgsEREW ERGE RG EGR', 'R RGTGARF gts tg strg s', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', NULL),
(2, 'veropulos555', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'veropulos', 'vero', 'fdvdvdfvvfvdfvge', 123321, 'skopje', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'xsxscdvvfvdcasdsad', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'dsadsfrgrsdfs', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'sadadfgsdfasda', 'dsadaefsdsdsa', 'sddsfrgwredadawdad', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', NULL),
(3, 'veropulos 3', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'veropulos', 'vero', 'ddfgdgdfgdfgdfgdf', 123456, 'bul mak brigada', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'fgfdgfdgdfg', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'grgdgdrgrgd', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'drgrdgrdgd', 'drgrdggrgyger', 'jujyjdrsrtst', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'Servisi'),
(4, 'veropulos 3', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'veropulossss', 'veroooo', 'gfgdgdfgfsfddsdgfvd', 70123456, 'k racin bb', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'fgdfghgghgjgg', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'gfsrssrgdg', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'fhgfhfgsfsrgfsr', 'tfhfcvbvcbvbcvb', 'nhnhnhvddgfcbvbv', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'Uslugi'),
(5, 'fgdfgdgdgd', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'sdffsdfds', 'fsdfs', 'dsfdsfsfsfs', 785678, 'hghfsdfdss', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'tgfgfhf', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'fhthtfhfthfth', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'fthfthftsdrsets', 'drthfhyjgjg', 'rdtdgthy', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'Servisi'),
(6, 'fdgdgdfgfd', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'retetert', 'rtree', 'treterterte', 147885, 'gfdgfgdgdgd', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'gfhfhfgfh', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'ghdrtrgthf', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'hfhfhddrrdrdy', 'ygjydrtstrd', 'ygjyjygcfggfhj', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'Servisi'),
(7, 'fdgdgdfgfd', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'retetert', 'rtree', 'treterterte', 147885, 'gfdgfgdgdgd', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'gfhfhfgfh', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'ghdrtrgthf', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'hfhfhddrrdrdy', 'ygjydrtstrd', 'ygjyjygcfggfhj', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'Servisi'),
(8, 'jkjhfhhfghfhhfhh', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'gujgyjgyjgfyj', 'yyjytdr', 'ygjgyjgjgjg', 1234567, 'gjhjfytszfdfsfddgf', 'https://www.mssociety.org.uk/sites/default/files/Holidays%20-%20edited.jpg', 'hgjfyhfyfjy', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'hjfhdtdthdh', 'https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg', 'gyjgdtdhdh', 'jkijljlijlj', 'rdttdttyugu', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'Servisi'),
(9, 'Valve', 'https://developer.valvesoftware.com/w/images/thumb/d/d3/Csgo_logo.jpg/360px-Csgo_logo.jpg', 'Valve SOFTWARE', 'VaLve', 'Steam is our direct pipeline to customers. It began as a little sleeper project—a handy tool to update Counter-Strike—and morphed pretty quickly into the world\'s largest online gaming platform. Steam guarantees instant access to more than 1,800 game titles and connects its 35 million active users to each other—and to us. dfe', 1236987, 'Bellevue, WA 98009 ', 'https://jtmgames.files.wordpress.com/2011/09/csgo4.jpg', 'CS GO', 'http://media.moddb.com/images/games/1/1/217/Counter-Strike_Box.jpg', 'Half Life', 'https://cdn.ndtv.com/tech/gadgets/dota-2-official.jpg?output-quality=80', 'DOTA 2', 'We started with Half-Life®, the first-person, sci-fi shooter game that’s won more than 50 Game of the Year and a few "Best Game Ever" awards. We expanded the Half-Life franchise with a sequel and Half-Life 2 Episodes 1 and 2. Then we added the horror titles Left 4 Dead® and Left 4 Dead 2, the multiplayer combat franchise Team Fortress®, the #1 online action game Counter-Strike® and the award-winning puzzlers Portal™ and Portal 2. And we’re just getting warmed up.', 'magine working with super smart, super talented colleagues in a free-wheeling, innovative environment—no bosses, no middle management, no bureaucracy. Just highly motivated peers coming together to make cool stuff. It’s amazing what creative people can come up with when there’s nobody there telling them what to do.', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'Servisi'),
(10, 'Valve', 'https://developer.valvesoftware.com/w/images/thumb/d/d3/Csgo_logo.jpg/360px-Csgo_logo.jpg', 'Valve SOFTWARE', 'VaLve', 'Steam is our direct pipeline to customers. It began as a little sleeper project—a handy tool to update Counter-Strike—and morphed pretty quickly into the world\'s largest online gaming platform. Steam guarantees instant access to more than 1,800 game titles and connects its 35 million active users to each other—and to us. dfe', 1236987, 'Bellevue, WA 98009 ', 'https://jtmgames.files.wordpress.com/2011/09/csgo4.jpg', 'CS GO', 'https://jtmgames.files.wordpress.com/2011/09/csgo4.jpg', 'Half Life', 'https://cdn.ndtv.com/tech/gadgets/dota-2-official.jpg?output-quality=80', 'DOTA 2', 'We started with Half-Life®, the first-person, sci-fi shooter game that’s won more than 50 Game of the Year and a few "Best Game Ever" awards.', 'imagine working with super smart, super talented colleagues in a free-wheeling, innovative environment—no bosses, no middle management, no bureaucracy. Just highly motivated peers coming together to make cool stuff. It’s amazing what creative people can come up with when there’s nobody there telling them what to do.', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'https://www.google.com/', 'Servisi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `proekt`
--
ALTER TABLE `proekt`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `proekt`
--
ALTER TABLE `proekt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<!DOCTYPE html>
<html>
<head>
	
	<title>Treta strana</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">

</head>

<body>

<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');


$id = $_GET['id'];
$username = 'root';
$password = 'root';
$database_host = 'localhost';
$database_name = 'prv_proekt';
$database_type = 'mysql';

$connection = new PDO("$database_type:host=$database_host;dbname=$database_name", $username, $password);

$statement = $connection->prepare('SELECT * FROM proekt WHERE id = :id');

$statement->bindValue(':id', $id);

$statement->execute();

$result = $statement->fetch(PDO::FETCH_ASSOC);
?>


	<div class="navbar">	
		<ul>
			<li><a href="#">Doma</a></li>
			<li><a href="#za_nas">Za nas</a></li>
			<li><a href="#servisi_produkti">
			<?php 
            if($result['servisi_produkti'] == 'Servisi')
            {
               echo "SERVISI";
            }else
            {
               echo "PRODUKTI";
            }
            ?>  
			</a></li>
			<li><a href="#Kontakt">Kontakt</a></li>
		</ul>
	</div>

	<div class="title" style="background-image: url(<?php echo $result ['link_cover']; ?>);">
		
		<h1><?php echo $result ['naslov']; ?> </h1>
        <p><?php echo $result ['podnaslov']; ?></p>
			

		</div>
	


	<div class="about" id='za_nas'>

		<h3>Za nas</h3>

		<p><?php echo $result ['nesto_za_vas']; ?></p>
 	
 	
 	</div>

 	<div class="contact">
 		<p><b>Telefon</b></p>
 		<p><?php echo $result ['telefon']; ?></p>
 		<br>
 		<p><b>Lokacija</b></p>
 		<p><?php echo $result ['lokacija']; ?> </p>
 		

 	</div>

 	<div class="serilipro" id="servisi_produkti">
 		<h2><?php 
             if($result['servisi_produkti'] == 'Servisi')
            {
                echo "SERVISI";
            }else
            {
                echo "PRODUKTI";
            }
            ?> 
        </h2>

 			<div class="prv">
 				<img src="<?php echo $result ['url1']; ?>" >
 				<!-- <h4>Opis na produktot</h4> -->
 				<p><?php echo $result ['opis_url1']; ?></p>
 			</div>

 			<div class="vtor">
 				<img src="<?php echo $result ['url2']; ?>" >
 				<!-- <h4>Opis na produktot</h4> -->
 				<p><?php echo $result ['opis_url2']; ?></p>
 			</div>

 			<div class="tret">
 				<img src="<?php echo $result ['url3']; ?>"" >
 				<!-- <h4>Opis na produktot</h4> -->
 				<p><?php echo $result ['opis_url3']; ?> </p>
 			</div>
 	</div>

 	<div class="centar" id="Kontakt">
 		<h4>Kontakt</h4>

 		<div class="left">
 			<!-- <h3>Info za vas pred kontakt</h3> -->
 			<p><?php echo $result ['nesto_poveke']; ?></p>
 		</div>

 		<div class=" desen">
 			<label>Ime</label>
 			<input type="text" name="" placeholder="Vaseto ime"><br>
 			<label>meil</label>
 			<input type="email" name="" placeholder="Vasiot e-mail"><br>
 			<label>poraka</label>
 			<textarea cols="20" rows="5" placeholder="Vasata poraka"></textarea><br>
 			<button>Isprati</button>
 			
 		</div>	

 	</div>

 	<footer> 
 		<div class="l-fut">
 		<p><?php echo $result['najdole']; ?></p>
 		</div>

 		<div class="d-fut">
 			<a href="<?php echo $result['linked'];?>"> <i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a>
 			<a href="<?php echo $result['face'];?>"> <i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
 			<a href="<?php echo $result['twiter'];?>"> <i class="fa fa-twitter  fa-2x" aria-hidden="true"></i></a>
        	<a href="<?php echo $result['google'];?>"> <i class="fa fa-google-plus" aria-hidden="true"></i></a>
        

 		</div>

 	</footer>

</body>
</html>